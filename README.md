# README #

This repository contains the formalization of three drifferent Hoare logics for
reasoning about time bounds in Isabelle/HOL.



### Abstract ###

In this theory we study three different Hoare logics for reasoning about time bounds of
imperative programs and formalize them in Isabelle/HOL: a classical Hoare
like logic due to Nielson, a logic with potentials due to Carbonneaux et
al. and a separation logic following work by Atkey, Chaguérand
and Pottier. These logics are formally shown to be sound and complete.
Verification condition generators are developed and are shown sound and
complete too. We also consider variants of the systems where we abstract
from multiplicative constants in the running time bounds, thus supporting a
big-O style of reasoning.


### Structure of this repository ###


Theories to the four logics are located in the files LOGIC_Hoare, LOGIC_VCG and LOGIC_Example.

For Nielson:

| Filename  |  |
| --------- | ---------- |
| Nielson_Hoare |	formalizes the Hoare logic  |
| Nielson_VCG	| gives the formalization of the VCG  |
| Nielson_VCGi	| gives the formalization of the improved VCG   |
| Nielson_VCGi_completeness	| gives the completness proof of the improved VCG  |
| Nielson_Examples | contains some examples  |
| Nielson_Sqrt | contains a **nice example**: we show that computing the discrete square root by bisection has running time O(log x)  |


For the simple quantitative Hoare logic:

| Filename  |  |
| --------- | ---------- | 
| Quant_Hoare	| formalizes the simple quantitative Hoare logic  |
| Quant_VCG	| gives the formalization of its VCG  |
| Quant_Examples | contains some examples  |
| Nielson_Sqrt | contains a **nice example**: we show that computing the discrete square root by bisection has running time O(log x)  |


For the quantitative big-O style Hoare logic:

| Filename  |  |
| --------- | ---------- |
| QuantK_Hoare	| formalizes the simple quantitative Hoare logic  |
| QuantK_VCG	| gives the formalization of its VCG  |
| QuantK_Examples | contains some examples  |


And finally for the logic using separation logic:

| Filename  |              |
| --------- | ---------- |
| SepLog_Hoare	| formalizes the Hoare logic |
| SepLog_Examples | contains some examples |
| SepLogAdd/* |  contains additional theories for separation algebras |

