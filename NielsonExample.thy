theory NielsonExample
imports Nielson_VCG
begin


  
term "(2::nat) ^ 2"  
  
abbreviation "log n \<equiv> (THE k. n = (2::int) ^ k)"  
  
lemma Z: "1 + L < R \<Longrightarrow> R - L = 2 * 2 ^ k' \<Longrightarrow> (log (R - (L + R) div 2) = k') "  
proof -  
  assume "1 + L < R" "R - L = 2 * 2 ^ k'"
  then 
  have "(R - ((L + R) div 2)) = 2 ^ k'" sorry
  then show "(log (R - (L + R) div 2) = k')" sorry  
qed
  
lemma Y: "R - L = 2 *  2 ^ k \<Longrightarrow> log ((L + R) div 2 - L) = (k::nat)" sorry
    
definition "isqrt (m::nat) = (THE n::nat. n * n \<le> m \<and> m < (n+1) * (n+1))"    
  
lemma isqrtE: "n * n \<le> m \<Longrightarrow> m < (n+1) * (n+1) \<Longrightarrow> isqrt m = n"
  unfolding isqrt_def apply(rule the1_equality) 
  subgoal unfolding Ex1_def 
    apply(rule exI[where x=n]) apply safe
    subgoal for y  apply(cases "y<n")
      subgoal
        by (meson discrete mult_le_mono not_less order.strict_trans1)   
      apply(cases "y>n")
      subgoal
        by (meson discrete dual_order.strict_trans2 mult_le_mono not_less)   
      subgoal by auto
    done done
  subgoal by auto
  done  
  
    
  
lemma
  fixes C lb::acom
  assumes "True" and 
    I: "I = (%l s. s ''x'' = 2 ^ X \<and> (\<exists>k. (s ''r'' -1) - s ''l'' = 2 ^ k) \<and> 0 \<le> s ''l'' \<and> s ''l'' < s ''r'' \<and>
            s ''l'' * s ''l'' \<le> s ''x'' \<and> s ''x'' < s ''r'' * s ''r'' \<and> s ''m'' = 0
           )" and
    E: "E = (%s. 5 * log (s ''r'' - s ''l'') + 1 )" and
    S: "S = (%s. s(''m'':=0, ''l'':=(int (isqrt (nat (s ''x'')))), ''r'':=(1+int (isqrt (nat (s ''x'')))) ))" and
    lb: "(lb::acom) = ''m'' ::= (Div (Plus (V ''l'') (V ''r'')) (N 2)) ;; 
               (IF Not (Less (Times (V ''m'') (V ''m'')) (V ''x'')) 
                  THEN ''l'' ::= V ''m''
                  ELSE ''r'' ::= V ''m'');; (''m'' ::= N 0)" and
    C: "C = ''l''::= N 0 ;; (''m'' ::= N 0) ;; ''r''::= V ''x'';; ({(I,(S,E))} WHILE (Less (Plus (N 1) (V ''l'')) (V ''r'')) DO lb)"
  shows  "\<turnstile>\<^sub>Q {%l s. s ''x'' = 2 ^ X} strip C {time C \<Down> %l s. s ''x'' = 2 ^ X \<and> s ''r'' = s ''l'' + 1 \<and> nat (s ''l'') = isqrt (nat (s ''x'')) }"
  apply(rule vc_sound')
  unfolding C lb
  subgoal (* VC *) 
    apply auto
    subgoal (* I preservation *) 
      for s unfolding I apply safe apply simp apply simp 
      subgoal for k apply(cases k) apply simp subgoal for k' apply(rule exI[where x=k']) by simp done
      subgoal by auto 
      subgoal by auto
      subgoal apply auto sorry
      subgoal by auto
      subgoal by auto
    done
    subgoal unfolding E I apply auto subgoal for k apply(cases k) apply auto by(auto simp: Z) done
    subgoal unfolding I S by auto
    subgoal unfolding I apply auto done
    subgoal unfolding I E by auto 
    subgoal unfolding I apply auto sorry
    subgoal unfolding I apply auto sorry
    subgoal unfolding I apply auto sorry
    subgoal unfolding I apply auto sorry (* error *) 
    subgoal unfolding I E apply auto subgoal for k apply(cases k) apply (auto) by (auto simp: Y) done
    subgoal unfolding I S by auto
    subgoal unfolding I by auto 
    subgoal unfolding I by auto 
    subgoal for l s unfolding I apply auto apply(rule isqrtE[symmetric])  
       apply (metis Nat_Transfer.transfer_int_nat_functions(4) Nat_Transfer.transfer_nat_int_function_closures(9) add_nonneg_nonneg le_nat_iff nat_eq_iff2 power2_eq_square zero_eq_power2 zero_le zle_iff_zadd)
    proof (goal_cases)
      case (1 k)
      from 1(2,6) have s: "s ''r'' = s ''l'' + 1" by linarith  
      thm   1(7)[unfolded this] nat_mono_iff[symmetric]
      have "nat (2 ^ X) < nat( ((s ''l'') + 1) * ( (s ''l'') + 1))"
        apply(simp add: nat_mono_iff[symmetric])
        apply safe using 1(5) apply simp using 1(8)[unfolded s] by auto
      also have "\<dots> = (nat (s ''l'') + 1) * (nat (s ''l'') + 1)" using 1(5)
        by (smt Nat_Transfer.transfer_nat_int_functions(1) nat_mult_distrib transfer_nat_int_numerals(2))  
      finally show ?case .
    qed      
    subgoal unfolding I E by auto
    subgoal for l s unfolding I S apply auto
    proof (goal_cases)
      case (1 k)
      then have A: "s ''m'' = 0 " by auto
      from 1(2,6) have s: "s ''r'' = s ''l'' + 1" by linarith 
      have "isqrt (nat (2 ^ X)) = nat (s ''l'')"
        apply(rule isqrtE) subgoal using 1(5,7)
          by (simp add: le_nat_iff) 
          using 1(8) s 1(5)
          by (smt One_nat_def add.commute add_0_left add_Suc_right int_nat_eq le_nat_iff linorder_not_less of_nat_mult semiring_1_class.of_nat_simps(2) zero_le_power) 
      then have B: "s ''l'' = int (isqrt (nat (2 ^ X)))" using 1(5) by auto
      with s have C: "s ''r'' = 1 + int (isqrt (nat (2 ^ X)))" by auto
      from A B C show ?case by force
    qed
  done       
  subgoal apply(simp add: support_inv) done
  subgoal unfolding I apply(simp add: support_inv) done
  subgoal unfolding I apply (simp only: pre.simps)  apply auto done
  done
     
  
  

end